#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "Backend.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/hello/main.qml"_qs);

    qmlRegisterType<Backend>("backend", 1, 0, "BackEnd");
    engine.load(url);
    return app.exec();
}