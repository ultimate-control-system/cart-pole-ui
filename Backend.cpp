//
// Created by Magnus Kjelland on 02/10/2022.
//

#include "Backend.h"
#include <iostream>

Backend::Backend(QObject *parent) :
        QObject(parent)

{
    qDebug() << "iam alive";
    DomainParticipantQos participantQos;
    participantQos.name("Cart pole ui qt");
    _participant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);
    _subscriber = new DdsSubscriber<CartPoleFeedbackPubSubType>(_participant, "CartPoleFeedback");
    _subscriber->bindDataAvailableCallback([=](const CartPoleFeedback &msg, const SampleInfo &info) {
        _cartPosition = msg.cart_x();
        _poleAngle = -msg.pole_theta() - M_PI_2;
        emit feedbackChanged(_cartPosition,_poleAngle);
    });
    _subscriber->bindSubscriptionMatchedCallback([=](const SubscriptionMatchedStatus& info) {
        qDebug() << (_subscriber->connected() ? "Matched" : "Unmatched");
    });
    _setpointPublisher = new DdsPublisher<CartPoleFeedbackPubSubType>(_participant,"CartPoleSetpoint");
}


void Backend::init()
{

}

void Backend::start()
{

}

void Backend::stop()
{

}

void Backend::updateCartSetpoint(double newSetpoint) {
    CartPoleFeedback message;
    message.cart_x() = newSetpoint;
    _setpointPublisher->publish(message);
}

Backend::~Backend()
{
    delete _subscriber;
    _subscriber = nullptr;
}
