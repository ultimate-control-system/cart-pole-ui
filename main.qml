import QtQuick
import backend 1.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts
import QtQuick.Shapes 1.12

Window {
    id: root
    visible: true
    width: 800
    height: 600

    property string display_mode: 'Dark'
    property string display_primary: 'Indigo'
    property string display_accent: 'Pink'

    Material.theme: Material[display_mode] //'Light'
    Material.accent: Material[display_accent]//display_accent
    Material.primary: Material[display_primary]

    RowLayout {
        Button {
            text: "Start"
            onClicked: backend.start()
            enabled:false
        }
        Button {
            text: "Stop"
            onClicked: backend.stop()
            enabled:false
        }
    }
    //RowLayout {
        Slider {
            id:slider
            width: parent.width
            from: -5
            value: 0
            to: 5
            onPressedChanged: {
                // show slider Hover when pressed, hide otherwise
                if( pressed ) {
                    //console.log("slider pressed. show hover.")
                }
                else {
                    //console.log("slider released. hide hover." + value)
                    backend.updateCartSetpoint(value);
                }
            }
        }
    //}

    // Line
    Shape {
        width: 200
        height: 150
        anchors.centerIn: parent
        ShapePath {
            strokeWidth: 2
            strokeColor: Material.accent
            startX: -400; startY: 100
            PathLine { x: 600; y: 100 }
        }
    }
    Shape {
        anchors.centerIn: parent
        ShapePath {
            strokeWidth: 2
            strokeColor: Material.accent
            startX: -50 + backend.cartPosition*100; startY: -50
            PathLine { x: 50+ backend.cartPosition*100; y: -50 }
            PathLine { x: 50+ backend.cartPosition*100; y: 50 }
            PathLine { x: -50+ backend.cartPosition*100; y: 50 }
            PathLine { x: -50+ backend.cartPosition*100; y: -50 }
        }
        ShapePath {
            strokeWidth: 10
            strokeColor: Material.accent
            startX: 0 + backend.cartPosition*100; startY: 0
            PathLine { x: 200*Math.cos(backend.poleAngle) + backend.cartPosition*100; y: 200*Math.sin(backend.poleAngle) }
        }
    }

    BackEnd {
        id: backend
    }
    Component.onCompleted: {
        backend.init();
    }
}