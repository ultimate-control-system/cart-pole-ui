//
// Created by Magnus Kjelland on 02/10/2022.
//

#ifndef HELLO_BACKEND_H
#define HELLO_BACKEND_H


#include <QObject>
#include <QTimer>
#include <QDebug>
#include "DdsSubscriber.hpp"
#include "DdsPublisher.hpp"

#include "TorqueControl/TorqueControlPubSubTypes.h"
#include "CartPoleFeedback/CartPoleFeedbackPubSubTypes.h"
#include "HelloWorld/HelloWorldPubSubTypes.h"

#define _USE_MATH_DEFINES
#include <cmath>
using namespace eprosima::fastdds::dds;

class Backend : public QObject
{
Q_OBJECT
    Q_PROPERTY(double cartPosition MEMBER _cartPosition NOTIFY feedbackChanged)
    Q_PROPERTY(double poleAngle MEMBER _poleAngle NOTIFY feedbackChanged)
private:
    double _cartPosition;
    double _poleAngle;
public:
    explicit Backend(QObject *parent = nullptr);

    ~Backend() override;

    Q_INVOKABLE
    void init();

    Q_INVOKABLE
    void start();
    Q_INVOKABLE
    void stop();
    Q_INVOKABLE
    void updateCartSetpoint(double newSetpoint);

    //double velocity() const { return _velocity; }
    //double orientation() const { return _orientation; }

signals:
    void feedbackChanged(double cartPosition, double rodAngle);


private:
    QTimer _timer;
    DomainParticipant* _participant;
    DdsSubscriber<CartPoleFeedbackPubSubType>* _subscriber;
    DdsPublisher<CartPoleFeedbackPubSubType>* _setpointPublisher;
};


#endif //HELLO_BACKEND_H
